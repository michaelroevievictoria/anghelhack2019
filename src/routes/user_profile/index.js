import React from 'react';
import Layout from '../../components/Layout';
import UserProfile from './UserProfile';

function action() {
    return {
      component: (
        <Layout>
          <UserProfile />
        </Layout>
      ),
    };
  }
  
  export default action;