import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import avatarUrl from './hello.jpg';

import s from './UserProfile.css';

class UserProfile extends React.Component {
	state = {
		first_name: 'John',
		last_name: 'Doe',
		age: '28',
		contact_number: '09772365685',
		primary_address: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
		billing_address: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
		zip_code: '4102',
		mother_name: '',
		father_name: '',
		country: [
			{
				"country": "Afghanistan"
			},
			{
				"country": "Åland Islands"
			},
			{
				"country": "Albania"
			},
			{
				"country": "Algeria"
			},
			{
				"country": "American Samoa"
			},
			{
				"country": "Andorra"
			},
			{
				"country": "Angola"
			},
			{
				"country": "Anguilla"
			},
			{
				"country": "Antarctica"
			},
			{
				"country": "Antigua and Barbuda"
			},
			{
				"country": "Argentina"
			},
			{
				"country": "Armenia"
			},
			{
				"country": "Aruba"
			},
			{
				"country": "Australia"
			},
			{
				"country": "Austria"
			},
			{
				"country": "Azerbaijan"
			},
			{
				"country": "Bahamas"
			},
			{
				"country": "Bahrain"
			},
			{
				"country": "Bangladesh"
			},
			{
				"country": "Barbados"
			},
			{
				"country": "Belarus"
			},
			{
				"country": "Belgium"
			},
			{
				"country": "Belize"
			},
			{
				"country": "Benin"
			},
			{
				"country": "Bermuda"
			},
			{
				"country": "Bhutan"
			},
			{
				"country": "Bolivia, Plurinational State of"
			},
			{
				"country": "Bonaire, Sint Eustatius and Saba"
			},
			{
				"country": "Bosnia and Herzegovina"
			},
			{
				"country": "Botswana"
			},
			{
				"country": "Bouvet Island"
			},
			{
				"country": "Brazil"
			},
			{
				"country": "British Indian Ocean Territory"
			},
			{
				"country": "Brunei Darussalam"
			},
			{
				"country": "Bulgaria"
			},
			{
				"country": "Burkina Faso"
			},
			{
				"country": "Burundi"
			},
			{
				"country": "Cambodia"
			},
			{
				"country": "Cameroon"
			},
			{
				"country": "Canada"
			},
			{
				"country": "Cape Verde"
			},
			{
				"country": "Cayman Islands"
			},
			{
				"country": "Central African Republic"
			},
			{
				"country": "Chad"
			},
			{
				"country": "Chile"
			},
			{
				"country": "China"
			},
			{
				"country": "Christmas Island"
			},
			{
				"country": "Cocos (Keeling) Islands"
			},
			{
				"country": "Colombia"
			},
			{
				"country": "Comoros"
			},
			{
				"country": "Congo"
			},
			{
				"country": "Congo, the Democratic Republic of the"
			},
			{
				"country": "Cook Islands"
			},
			{
				"country": "Costa Rica"
			},
			{
				"country": "Côte d'Ivoire"
			},
			{
				"country": "Croatia"
			},
			{
				"country": "Cuba"
			},
			{
				"country": "Curaçao"
			},
			{
				"country": "Cyprus"
			},
			{
				"country": "Czech Republic"
			},
			{
				"country": "Denmark"
			},
			{
				"country": "Djibouti"
			},
			{
				"country": "Dominica"
			},
			{
				"country": "Dominican Republic"
			},
			{
				"country": "Ecuador"
			},
			{
				"country": "Egypt"
			},
			{
				"country": "El Salvador"
			},
			{
				"country": "Equatorial Guinea"
			},
			{
				"country": "Eritrea"
			},
			{
				"country": "Estonia"
			},
			{
				"country": "Ethiopia"
			},
			{
				"country": "Falkland Islands (Malvinas)"
			},
			{
				"country": "Faroe Islands"
			},
			{
				"country": "Fiji"
			},
			{
				"country": "Finland"
			},
			{
				"country": "France"
			},
			{
				"country": "French Guiana"
			},
			{
				"country": "French Polynesia"
			},
			{
				"country": "French Southern Territories"
			},
			{
				"country": "Gabon"
			},
			{
				"country": "Gambia"
			},
			{
				"country": "Georgia"
			},
			{
				"country": "Germany"
			},
			{
				"country": "Ghana"
			},
			{
				"country": "Gibraltar"
			},
			{
				"country": "Greece"
			},
			{
				"country": "Greenland"
			},
			{
				"country": "Grenada"
			},
			{
				"country": "Guadeloupe"
			},
			{
				"country": "Guam"
			},
			{
				"country": "Guatemala"
			},
			{
				"country": "Guernsey"
			},
			{
				"country": "Guinea"
			},
			{
				"country": "Guinea-Bissau"
			},
			{
				"country": "Guyana"
			},
			{
				"country": "Haiti"
			},
			{
				"country": "Heard Island and McDonald Islands"
			},
			{
				"country": "Holy See (Vatican City State)"
			},
			{
				"country": "Honduras"
			},
			{
				"country": "Hong Kong"
			},
			{
				"country": "Hungary"
			},
			{
				"country": "Iceland"
			},
			{
				"country": "India"
			},
			{
				"country": "Indonesia"
			},
			{
				"country": "Iran, Islamic Republic of"
			},
			{
				"country": "Iraq"
			},
			{
				"country": "Ireland"
			},
			{
				"country": "Isle of Man"
			},
			{
				"country": "Israel"
			},
			{
				"country": "Italy"
			},
			{
				"country": "Jamaica"
			},
			{
				"country": "Japan"
			},
			{
				"country": "Jersey"
			},
			{
				"country": "Jordan"
			},
			{
				"country": "Kazakhstan"
			},
			{
				"country": "Kenya"
			},
			{
				"country": "Kiribati"
			},
			{
				"country": "Korea, Democratic People's Republic of"
			},
			{
				"country": "Korea, Republic of"
			},
			{
				"country": "Kuwait"
			},
			{
				"country": "Kyrgyzstan"
			},
			{
				"country": "Lao People's Democratic Republic"
			},
			{
				"country": "Latvia"
			},
			{
				"country": "Lebanon"
			},
			{
				"country": "Lesotho"
			},
			{
				"country": "Liberia"
			},
			{
				"country": "Libya"
			},
			{
				"country": "Liechtenstein"
			},
			{
				"country": "Lithuania"
			},
			{
				"country": "Luxembourg"
			},
			{
				"country": "Macao"
			},
			{
				"country": "Macedonia, the former Yugoslav Republic of"
			},
			{
				"country": "Madagascar"
			},
			{
				"country": "Malawi"
			},
			{
				"country": "Malaysia"
			},
			{
				"country": "Maldives"
			},
			{
				"country": "Mali"
			},
			{
				"country": "Malta"
			},
			{
				"country": "Marshall Islands"
			},
			{
				"country": "Martinique"
			},
			{
				"country": "Mauritania"
			},
			{
				"country": "Mauritius"
			},
			{
				"country": "Mayotte"
			},
			{
				"country": "Mexico"
			},
			{
				"country": "Micronesia, Federated States of"
			},
			{
				"country": "Moldova, Republic of"
			},
			{
				"country": "Monaco"
			},
			{
				"country": "Mongolia"
			},
			{
				"country": "Montenegro"
			},
			{
				"country": "Montserrat"
			},
			{
				"country": "Morocco"
			},
			{
				"country": "Mozambique"
			},
			{
				"country": "Myanmar"
			},
			{
				"country": "Namibia"
			},
			{
				"country": "Nauru"
			},
			{
				"country": "Nepal"
			},
			{
				"country": "Netherlands"
			},
			{
				"country": "New Caledonia"
			},
			{
				"country": "New Zealand"
			},
			{
				"country": "Nicaragua"
			},
			{
				"country": "Niger"
			},
			{
				"country": "Nigeria"
			},
			{
				"country": "Niue"
			},
			{
				"country": "Norfolk Island"
			},
			{
				"country": "Northern Mariana Islands"
			},
			{
				"country": "Norway"
			},
			{
				"country": "Oman"
			},
			{
				"country": "Pakistan"
			},
			{
				"country": "Palau"
			},
			{
				"country": "Palestinian Territory, Occupied"
			},
			{
				"country": "Panama"
			},
			{
				"country": "Papua New Guinea"
			},
			{
				"country": "Paraguay"
			},
			{
				"country": "Peru"
			},
			{
				"country": "Philippines"
			},
			{
				"country": "Pitcairn"
			},
			{
				"country": "Poland"
			},
			{
				"country": "Portugal"
			},
			{
				"country": "Puerto Rico"
			},
			{
				"country": "Qatar"
			},
			{
				"country": "Réunion"
			},
			{
				"country": "Romania"
			},
			{
				"country": "Russian Federation"
			},
			{
				"country": "Rwanda"
			},
			{
				"country": "Saint Barthélemy"
			},
			{
				"country": "Saint Helena, Ascension and Tristan da Cunha"
			},
			{
				"country": "Saint Kitts and Nevis"
			},
			{
				"country": "Saint Lucia"
			},
			{
				"country": "Saint Martin (French part)"
			},
			{
				"country": "Saint Pierre and Miquelon"
			},
			{
				"country": "Saint Vincent and the Grenadines"
			},
			{
				"country": "Samoa"
			},
			{
				"country": "San Marino"
			},
			{
				"country": "Sao Tome and Principe"
			},
			{
				"country": "Saudi Arabia"
			},
			{
				"country": "Senegal"
			},
			{
				"country": "Serbia"
			},
			{
				"country": "Seychelles"
			},
			{
				"country": "Sierra Leone"
			},
			{
				"country": "Singapore"
			},
			{
				"country": "Sint Maarten (Dutch part)"
			},
			{
				"country": "Slovakia"
			},
			{
				"country": "Slovenia"
			},
			{
				"country": "Solomon Islands"
			},
			{
				"country": "Somalia"
			},
			{
				"country": "South Africa"
			},
			{
				"country": "South Georgia and the South Sandwich Islands"
			},
			{
				"country": "South Sudan"
			},
			{
				"country": "Spain"
			},
			{
				"country": "Sri Lanka"
			},
			{
				"country": "Sudan"
			},
			{
				"country": "Suriname"
			},
			{
				"country": "Svalbard and Jan Mayen"
			},
			{
				"country": "Swaziland"
			},
			{
				"country": "Sweden"
			},
			{
				"country": "Switzerland"
			},
			{
				"country": "Syrian Arab Republic"
			},
			{
				"country": "Taiwan, Province of China"
			},
			{
				"country": "Tajikistan"
			},
			{
				"country": "Tanzania, United Republic of"
			},
			{
				"country": "Thailand"
			},
			{
				"country": "Timor-Leste"
			},
			{
				"country": "Togo"
			},
			{
				"country": "Tokelau"
			},
			{
				"country": "Tonga"
			},
			{
				"country": "Trinidad and Tobago"
			},
			{
				"country": "Tunisia"
			},
			{
				"country": "Turkey"
			},
			{
				"country": "Turkmenistan"
			},
			{
				"country": "Turks and Caicos Islands"
			},
			{
				"country": "Tuvalu"
			},
			{
				"country": "Uganda"
			},
			{
				"country": "Ukraine"
			},
			{
				"country": "United Arab Emirates"
			},
			{
				"country": "United Kingdom"
			},
			{
				"country": "United States"
			},
			{
				"country": "United States Minor Outlying Islands"
			},
			{
				"country": "Uruguay"
			},
			{
				"country": "Uzbekistan"
			},
			{
				"country": "Vanuatu"
			},
			{
				"country": "Venezuela, Bolivarian Republic of"
			},
			{
				"country": "Viet Nam"
			},
			{
				"country": "Virgin Islands, British"
			},
			{
				"country": "Virgin Islands, U.S."
			},
			{
				"country": "Wallis and Futuna"
			},
			{
				"country": "Western Sahara"
			},
			{
				"country": "Yemen"
			},
			{
				"country": "Zambia"
			},
			{
				"country": "Zimbabwe"
			}
		]
	};
  render() {

    return (
			<React.Fragment>
				<Container maxWidth="sm">
				<Grid container justify="center" alignItems="center">
					<Avatar style={{ width: 170, height: 170, marginTop: 25, marginBottom: 25 }} alt="Remy Sharp" src={ avatarUrl } />
				</Grid>
				<form className="" noValidate autoComplete="off">
					<TextField
						fullWidth
						id="standard-first-name"
						label="First Name"
						className=""
						value={ this.state.first_name }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-last-name"
						label="Last Name"
						className=""
						value={ this.state.last_name }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-age"
						label="Age"
						className=""
						value={ this.state.age }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-contact-number"
						label="Contact Number"
						className=""
						value={ this.state.contact_number }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-primary-address"
						label="Primary Address"
						className=""
						value={ this.state.primary_address }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-billing-address"
						label="Billing Address"
						className=""
						value={ this.state.billing_address }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-zip-code"
						label="Zip Code"
						className=""
						value={ this.state.zip_code }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-mothers-name"
						label="Mother's Name"
						className=""
						value={ this.state.mother_name }
						margin="normal"
					/>
					<TextField
						fullWidth
						id="standard-father-name"
						label="Father's Name"
						className=""
						value={ this.state.father_name }
						margin="normal"
					/>
					<FormControl fullWidth className="" style={{ marginTop: 25 }}>
						<InputLabel shrink htmlFor="country-dropdown-label-placeholder">
							Country
						</InputLabel>
						<Select
							native
							value="Philippines"
						>							
						{
							this.state.country.map((value, index) => {
								return(
									<option key={ index } value={ value.country }>{ value.country }</option>
								)
							})	
						}
						</Select>
					</FormControl>
					<Button fullWidth variant="contained" color="primary" style={{marginTop: 25, marginBottom: 25}} className={ s.button_css }>
						Save
					</Button>
				</form>
				</Container>
			</React.Fragment>
    );
  }
}

export default withStyles(s)(UserProfile);
